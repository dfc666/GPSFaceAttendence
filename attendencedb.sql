-- MySQL dump 10.13  Distrib 5.6.44, for Win64 (x86_64)
--
-- Host: localhost    Database: attendencedb
-- ------------------------------------------------------
-- Server version	5.6.44

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '课程id',
  `course_code` int(6) DEFAULT NULL COMMENT '课程唯一课程号',
  `img_path` varchar(255) DEFAULT NULL COMMENT '图片网络路径',
  `name` varchar(64) NOT NULL COMMENT '课程名',
  `teacher_name` varchar(16) DEFAULT NULL COMMENT '任课老师',
  `start_time` varchar(64) DEFAULT NULL COMMENT '课程的开始时间',
  `end_time` varchar(64) DEFAULT NULL COMMENT '课程的结束时间',
  `today` varchar(16) DEFAULT NULL COMMENT '课程星期几',
  `room` varchar(64) DEFAULT NULL COMMENT '上课教室',
  `longitude` varchar(64) DEFAULT NULL COMMENT '教室的经度坐标',
  `latitude` varchar(64) DEFAULT NULL COMMENT '教室的纬度坐标',
  `signin_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '课程的签到状态',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `classess` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `course_code` (`course_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` VALUES (6,8816,'http://192.168.2.109:6060/attendence/courseCode8816/courseImg/963A6F95.png','数学','张老师','15:14','15:49','星期一','8教-201','113.12602','22.933378',1,'2020-03-09 16:06:26','2020-04-10 07:04:10',NULL),(7,3964,'http://192.168.2.109:6060/attendence/courseCode3964/courseImg/E8A49AEB.png','语文','陆老师','00:00','02:00','星期二','7教-301','113.132602','22.939373',1,'2020-03-09 16:06:26','2020-04-10 07:05:46',NULL),(8,5346,NULL,'英语','李老师','20:00','00:00','星期三','6教-101','113.132602','22.939373',1,'2020-03-09 16:06:26','2020-04-10 07:33:06',NULL),(26,9212,'http://192.168.2.109:6060/attendence/courseCode9212/courseImg/6E80E5A61.png','物理','曾老师','13:00','22:00','星期五','8教-101','113.12606','22.933354',1,'2020-03-19 10:25:42','2020-04-10 06:43:04',NULL),(30,8247,'http://192.168.2.109:6060/attendence/courseCode8247/courseImg/U05vFmlRSO.png','Java','杨老师','08:00','10:00','星期二','9-201','113.126132','22.933372',1,'2020-03-16 17:25:38','2020-04-10 05:33:28',NULL),(31,3266,'http://192.168.2.109:6060/attendence/courseCode3266/courseImg/PYdqt3Go6l.png','电子','钟老师','10:00','12:00','星期二','2教101','113.126191','22.933218',0,'2020-03-16 17:39:34','2020-04-10 06:43:14',NULL),(32,4297,'http://192.168.2.109:6060/attendence/courseCode4297/courseImg/CJn1hJc3VM.png','历史','李老师','22:30','23:59','星期二','2-101','113.132592','22.939338',1,'2020-03-16 17:54:25','2020-04-10 15:18:35',NULL),(33,7465,'http://192.168.2.109:6060/attendence/courseCode7465/courseImg/QAD4fgnRMX.png','心理学','唐老师','14:00','17:30','星期三','8-137','113.132589','22.939345',1,'2020-03-17 18:50:44','2020-04-10 05:35:38',NULL),(41,4564,'http://192.168.2.109:6060/attendence/courseCode4564/courseImg/ThTlJGH6Rs.png','电子信息','蔡老师','13:00','15:00','星期四','8-201','113.126031','22.933346',1,'2020-03-19 10:33:35','2020-04-10 06:43:38',NULL),(47,9738,'http://192.168.2.109:6060/attendence/courseCode9738/courseImg/r2IvkNL0VA.png','天文','苏老师','10:00','19:00','星期四','3-102','113.132602','22.939373',1,'2020-03-28 05:44:44','2020-04-10 05:36:36',NULL),(51,4666,'http://192.168.2.109:6060/attendence/courseCode4666/courseImg/5aVd30l6AL.png','数据结构','田老师','15:00','16:30','星期三','3-201','113.132565','22.939448',0,'2020-04-10 07:18:36','2020-04-10 14:31:11',NULL);
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_resource`
--

DROP TABLE IF EXISTS `course_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `course_code` int(11) DEFAULT NULL COMMENT '外键课程号',
  `resource_name` varchar(64) DEFAULT NULL COMMENT '资源名',
  `resource_comment` varchar(255) DEFAULT NULL COMMENT '资源备注',
  `resource_net_path` varchar(255) DEFAULT NULL COMMENT '资源网络路径',
  `resource_local_path` varchar(255) DEFAULT NULL COMMENT '资源服务器路径',
  `resource_size` varchar(64) DEFAULT NULL COMMENT '资源大小',
  `resource_type` varchar(16) DEFAULT NULL COMMENT '资源类型',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `fk_cc` (`course_code`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_resource`
--

LOCK TABLES `course_resource` WRITE;
/*!40000 ALTER TABLE `course_resource` DISABLE KEYS */;
INSERT INTO `course_resource` VALUES (37,8247,'推理','顺客隆','http://192.168.2.109:6060/attendence/courseCode8247/courseDocument/2020_3_18_746637711.mp3','E://profile/courseCode8247/courseDocument/2020_3_18_746637711.mp3','3.50M','mp3','2020-03-18 03:10:32','2020-03-18 03:10:32'),(39,4297,'周杰伦','千里之外\r\n','http://192.168.2.109:6060/attendence/courseCode4297/courseDocument/2020_3_18_2082976999.mp3','E://profile/courseCode4297/courseDocument/2020_3_18_2082976999.mp3','3.91M','mp3','2020-03-18 10:33:11','2020-03-18 10:33:11'),(40,8247,'测试图片','哈哈哈','http://192.168.2.109:6060/attendence/courseCode8247/courseDocument/2020_3_18_1479680515.jpg','E://profile/courseCode8247/courseDocument/2020_3_18_1479680515.jpg','800.87K','jpg','2020-03-18 12:50:38','2020-03-18 12:50:38'),(44,4297,'测试pdf','哈哈哈','http://192.168.2.109:6060/attendence/courseCode4297/courseDocument/2020_3_19_1652860147.pdf','E://profile/courseCode4297/courseDocument/2020_3_19_1652860147.pdf','71.50K','pdf','2020-03-19 11:06:45','2020-03-19 11:06:45'),(45,4564,'继续测试',NULL,'http://192.168.2.109:6060/attendence/courseCode4564/courseDocument/2020_3_19_1596880597.pdf','E://profile/courseCode4564/courseDocument/2020_3_19_1596880597.pdf','71.50K','pdf','2020-03-19 14:42:05','2020-03-19 14:42:05'),(48,9738,'测试图片','这是地球图片','http://192.168.2.109:6060/attendence/courseCode9738/courseDocument/2020_3_28_782859858.jpg','E://profile/courseCode9738/courseDocument/2020_3_28_782859858.jpg','132.75K','jpg','2020-03-28 06:03:20','2020-03-28 06:03:20'),(52,4666,'图片资源1','图片描述1','http://192.168.2.109:6060/attendence/courseCode4666/courseDocument/2020_4_10_917065473.jpg','E://profile/courseCode4666/courseDocument/2020_4_10_917065473.jpg','800.87K','jpg','2020-04-10 07:19:30','2020-04-10 07:19:30');
/*!40000 ALTER TABLE `course_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_result`
--

DROP TABLE IF EXISTS `course_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `course_code` int(11) DEFAULT NULL COMMENT '外键课程号',
  `number` varchar(32) DEFAULT NULL COMMENT '外键学号',
  `face_img` varchar(255) DEFAULT NULL COMMENT '人脸图网络路径',
  `face_token` varchar(255) DEFAULT NULL COMMENT '人脸标识',
  `face_img_local` varchar(255) DEFAULT NULL COMMENT '人脸图服务器路径',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '1' COMMENT '软删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_result`
--

LOCK TABLES `course_result` WRITE;
/*!40000 ALTER TABLE `course_result` DISABLE KEYS */;
INSERT INTO `course_result` VALUES (1,8816,'123456',NULL,NULL,NULL,1,'2020-03-03 12:18:48','2020-03-15 15:49:22'),(3,3964,'123456',NULL,NULL,NULL,1,'2020-03-03 12:18:48','2020-03-15 15:49:23'),(6,5346,'123456',NULL,NULL,NULL,1,'2020-03-03 12:18:48','2020-03-15 15:49:24'),(8,8816,'666666',NULL,NULL,NULL,0,'2020-03-03 12:18:48','2020-03-29 03:30:47'),(9,3964,'666666','http://192.168.2.109:6060/attendence/courseCode3964/student/学号__66666659405995-c192-49b2-8651-def0be7d644c.png','20fba881226e560cec32d59343d4e83c',NULL,1,'2020-03-03 12:18:48','2020-03-29 17:47:18'),(10,5346,'666666','http://192.168.2.109:6060/attendence/courseCode5346/student/学号666666__4c7af6d4-3e4b-47e7-9734-8894cf2f0566.png','0f319b07f70ba34e048868f933f7c046','E://profile/courseCode5346/student/学号666666__4c7af6d4-3e4b-47e7-9734-8894cf2f0566.png',1,'2020-03-03 12:18:48','2020-03-23 16:07:54'),(13,9212,'666666','',NULL,NULL,1,'2020-03-09 18:19:23','2020-03-18 10:46:18'),(14,8247,'666666','http://192.168.2.109:6060/attendence/courseCode8247/student/学号__666666dfaa72ba-c311-4bdb-901d-223e169ce0c8.png',NULL,NULL,1,'2020-03-18 09:10:44','2020-03-18 09:10:44'),(15,4297,'666666','http://192.168.2.109:6060/attendence/courseCode4297/student/学号__666666f7558859-a0bd-4ac7-989e-1b8c09cc577c.png','e129a769a75540c79ef408341687fe51',NULL,1,'2020-03-18 10:37:23','2020-03-28 05:15:28'),(16,7465,'666666','http://192.168.2.109:6060/attendence/courseCode7465/student/学号666666__0bcfb445-91df-42fd-873e-a7f9c97bc34e.png','20fba881226e560cec32d59343d4e83c','E://profile/courseCode7465/student/学号666666__0bcfb445-91df-42fd-873e-a7f9c97bc34e.png',1,'2020-03-21 08:27:37','2020-03-21 10:14:22'),(17,4564,'666666','http://192.168.2.109:6060/attendence/courseCode4564/student/学号666666__c82cf2f7-a1ee-41f4-8992-fbe1078dbf48.png','e129a769a75540c79ef408341687fe51','E://profile/courseCode4564/student/学号666666__c82cf2f7-a1ee-41f4-8992-fbe1078dbf48.png',0,'2020-03-28 04:38:24','2020-03-28 04:54:11'),(18,4666,'666666','http://192.168.2.109:6060/attendence/courseCode4666/student/学号666666__bf831c3b-3128-4e2b-95ca-244029f7ab36.png','12b4351b5322c3f5372242da3950d780','E://profile/courseCode4666/student/学号666666__bf831c3b-3128-4e2b-95ca-244029f7ab36.png',1,'2020-04-10 13:51:29','2020-04-10 13:51:29');
/*!40000 ALTER TABLE `course_result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (72),(72),(72),(72);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `signin`
--

DROP TABLE IF EXISTS `signin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `signin` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `stu_name` varchar(16) DEFAULT NULL,
  `number` varchar(16) DEFAULT NULL COMMENT '外键学号',
  `ip` varchar(64) DEFAULT NULL COMMENT '签到机的IP地址',
  `classes` varchar(255) DEFAULT NULL,
  `course_code` int(6) DEFAULT NULL COMMENT '外键课程号',
  `course_name` varchar(64) DEFAULT NULL,
  `teacher_name` varchar(64) DEFAULT NULL,
  `room` varchar(32) DEFAULT NULL,
  `week_day` varchar(16) DEFAULT NULL COMMENT '星期几',
  `signin_time` varchar(128) DEFAULT NULL COMMENT '学生本次签到时间',
  `signin_date` date DEFAULT NULL COMMENT '学生签到日期',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `signin_result` varchar(32) DEFAULT NULL COMMENT '签到结果',
  `signin_status` tinyint(1) DEFAULT '1' COMMENT '签到状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `signin`
--

LOCK TABLES `signin` WRITE;
/*!40000 ALTER TABLE `signin` DISABLE KEYS */;
INSERT INTO `signin` VALUES (13,'小明','666666','192.168.2.105','计算机1班',8816,'数学','张老师','8教-201',NULL,'4:34','2020-03-04','2020-03-04 12:34:46','2020-03-04 12:34:46','签到成功',1),(14,'小明','666666','192.168.2.105','计算机1班',8816,'数学','张老师','8教-201',NULL,'4:36','2020-03-04','2020-03-04 12:36:07','2020-03-04 12:36:07','签到成功',1),(15,'小明','666666','192.168.2.105','计算机1班',8816,'数学','张老师','8教-201',NULL,'5:11','2020-03-04','2020-03-04 13:11:14','2020-03-29 16:24:51','签到成功',1),(16,'小明','666666','192.168.2.105','计算机1班',8816,'数学','张老师','8教-201',NULL,'5:11','2020-03-04','2020-03-04 13:11:27','2020-03-04 13:11:27','签到成功',1),(18,'小明','666666','192.168.2.105','计算机1班',8816,'数学','张老师','8教-201',NULL,'03:05','2020-03-05','2020-03-05 13:23:34','2020-03-29 16:24:56','签到成功',1),(20,'小明','666666','192.168.2.105','计算机1班',5346,'英语','李老师','6教-101',NULL,'00:36','2020-03-21','2020-03-05 16:45:19','2020-04-10 07:59:11','签到成功,迟到36分钟',2),(23,'小明','666666','192.168.2.105','计算机1班',5346,'英语','李老师','6教-101',NULL,'00:18','2020-03-20','2020-03-08 15:18:17','2020-04-10 07:58:15','签到成功,迟到18分钟',2),(46,'小明','666666','192.168.2.105','计算机1班',5346,'英语','李老师','6教-101',NULL,'23:26','2020-03-31','2020-04-29 16:26:51','2020-04-10 07:58:11','签到成功',1),(49,'小明','666666','192.168.2.105','计算机1班',3964,'语文','陆老师','7教-301',NULL,'01:48','2020-03-30','2020-03-29 17:48:32','2020-03-29 17:48:32','签到成功,迟到48分钟',2),(50,'小明','666666','192.168.2.105','计算机1班',5346,'英语','李老师','6教-101',NULL,'00:10','2020-04-03','2020-03-30 06:01:51','2020-04-10 08:01:01','签到成功,迟到10分钟',2),(54,'小明','666666','192.168.2.105','计算机6班',4297,'历史','李老师','2-101',NULL,'23:37','2020-04-10','2020-04-10 15:37:11','2020-04-10 16:07:47','签到成功,迟到67分钟',2),(55,'小明','666666','192.168.2.103','计算机6班',4297,'历史','李老师','2-101',NULL,'16:25','2020-04-12','2020-04-12 08:25:25','2020-04-12 08:25:25','签到成功',1);
/*!40000 ALTER TABLE `signin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `classes` varchar(255) DEFAULT NULL COMMENT '班级',
  `name` varchar(32) DEFAULT NULL COMMENT '姓名',
  `number` varchar(16) DEFAULT NULL COMMENT '学号',
  `openid` varchar(128) DEFAULT NULL COMMENT '微信或QQ标识值',
  `call` varchar(128) DEFAULT NULL COMMENT '联系方式',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (1,'计算机6班','小华','123456','asd',NULL),(9,'计算机6班','小明','666666','0BC89AB4B775441D7E82BF9B75973334',NULL),(11,'计算机5班','小华','162011','A255D8E9B642AACA3332E3F5B715B47B',NULL);
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher_login`
--

DROP TABLE IF EXISTS `teacher_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `login_code` varchar(16) DEFAULT NULL COMMENT '教师登录码',
  `is_deleted` int(11) DEFAULT '1' COMMENT '软删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher_login`
--

LOCK TABLES `teacher_login` WRITE;
/*!40000 ALTER TABLE `teacher_login` DISABLE KEYS */;
INSERT INTO `teacher_login` VALUES (1,'65987465',1,'2020-03-24 17:44:43','2020-03-24 17:44:54');
/*!40000 ALTER TABLE `teacher_login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(64) DEFAULT NULL COMMENT '管理员账户',
  `password` varchar(64) DEFAULT NULL COMMENT '管理员密码',
  `username` varchar(32) DEFAULT NULL COMMENT '用户名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'123','202cb962ac59075b964b07152d234b70','hahaha');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-23 22:23:45
