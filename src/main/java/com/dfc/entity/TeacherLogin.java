package com.dfc.entity;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * @author: zsh
 * @Date:16:13 2018/5/5
 * @Description: 教师登录码类类
 */

@Entity
@Table(name = "teacher_login")
@Data
@EntityListeners(AuditingEntityListener.class)
public class TeacherLogin {


    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "login_code",length = 16)
    private  String  loginCode;

    @Column(name = "is_deleted",insertable = false,columnDefinition = "int default 1")
    private Integer isDeleted;

    @CreatedDate
    @Column(name = "create_time")
    private Date createTime;

    @LastModifiedDate
    @Column(name = "update_time")
    private Date updateTime;


}
