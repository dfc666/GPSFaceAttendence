package com.dfc.entity;

import lombok.Data;

@Data
public class ResourcePath {

    private  String netPath;

    private  String localPath;

    public String getNetPath() {
        return netPath;
    }

    public void setNetPath(String netPath) {
        this.netPath = netPath;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }
}
