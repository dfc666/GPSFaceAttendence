package com.dfc.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * @author: zsh
 * @Date:16:13 2018/5/5
 * @Description: 学生信息类
 */

@Entity
@Table(name = "course_result")
@Data
@EntityListeners(AuditingEntityListener.class)
public class CourseResult  {
//    private static final long serialVersionUID=1L;


    public CourseResult() {
    }

    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 学生的学号
     */
    @Column(name = "number",length = 32)
    private String number;


    /**
     * 课程数字代码
     */
    @Column(name = "course_code",length = 6)
    private Integer courseCode;


    @Column(name = "is_deleted")
    private Integer isDeleted;


    @Column(name = "face_img")
    private String faceImg;

    @Column(name = "face_img_local")
    private String faceImgLocalPath;


    @Column(name = "face_token")
    private String faceToken;

    @CreatedDate
    @Column(name = "create_time")
    private Date createTime;

    @LastModifiedDate
    @Column(name = "update_time")
    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(Integer courseCode) {
        this.courseCode = courseCode;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getFaceImg() {
        return faceImg;
    }

    public void setFaceImg(String faceImg) {
        this.faceImg = faceImg;
    }

    public String getFaceImgLocalPath() {
        return faceImgLocalPath;
    }

    public void setFaceImgLocalPath(String faceImgLocalPath) {
        this.faceImgLocalPath = faceImgLocalPath;
    }

    public String getFaceToken() {
        return faceToken;
    }

    public void setFaceToken(String faceToken) {
        this.faceToken = faceToken;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CourseResult)) return false;

        CourseResult that = (CourseResult) o;

        return number != null ? number.equals(that.number) : that.number == null;

    }

    @Override
    public int hashCode() {
        return number != null ? number.hashCode() : 0;
    }
}
