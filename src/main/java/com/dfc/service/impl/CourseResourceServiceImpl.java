package com.dfc.service.impl;

import com.dfc.api.StorageApi;
import com.dfc.dao.CourseResourceDao;
import com.dfc.entity.CourseResource;
import com.dfc.entity.Result;
import com.dfc.service.CourseResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CourseResourceServiceImpl implements CourseResourceService {
    @Autowired
    private CourseResourceDao courseResourceDao;

    @Autowired
    private StorageApi storageApi;

    @Override
    public CourseResource saveResource(CourseResource courseResource) {
       return courseResourceDao.save(courseResource);
    }

    @Override
    public List<CourseResource> findByCourseCode(Integer courseCode) {
        return  courseResourceDao.findByCourseCode(courseCode);

    }

    @Override
    public void deleteResource(Integer id) {
      courseResourceDao.deleteById(id);

    }

    @Override
    public CourseResource findResource(Integer id) {
        return courseResourceDao.findById(id).get();
    }


}
