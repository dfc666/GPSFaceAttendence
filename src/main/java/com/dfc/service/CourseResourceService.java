package com.dfc.service;

import com.dfc.dao.CourseResourceDao;
import com.dfc.entity.CourseResource;
import com.dfc.entity.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CourseResourceService {

    @Transactional
    CourseResource saveResource(CourseResource courseResource);

    @Transactional
    List<CourseResource>  findByCourseCode(Integer courseCode);

    @Transactional
    void deleteResource(Integer id);

    @Transactional
    CourseResource findResource(Integer id);

}
