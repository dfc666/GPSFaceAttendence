package com.dfc.controller;

import com.alibaba.fastjson.JSON;
import com.dfc.WebConfig;
import com.dfc.api.StorageApi;
import com.dfc.entity.*;
import com.dfc.service.CourseResourceService;
import com.dfc.service.CourseResultService;
import com.dfc.service.CourseService;
import com.dfc.service.StudentService;
import com.dfc.util.BaseCommonUtil;
import com.dfc.util.ImageUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.dfc.constant.DataConstant.PATH_SPLIT;


/**
 * @author: zsh
 * @Date:22:02 2018/5/9
 * @Description:
 */
@RestController
@Slf4j
@RequestMapping("/courseResource")
public class CourseResourceController {

//    @Value("${file-service.profile}")
//    private String homePath;
    @Resource
    CourseService courseService;

    @Autowired
    private CourseResultService courseResultService;

    @Autowired
    private StorageApi storageApi;

    @Autowired
    private StudentService studentService;

    @Autowired
    private CourseResourceService courseResourceService;


//    @Transactional
//    @RequestMapping(value = "/uploadResource", method = RequestMethod.POST)
//    public Result uploadResource(Integer courseCode,String showName,String myComment,HttpServletRequest request,String base64,String ext)  {
//        Result result = new Result<CourseResource>();
//
//         String dictoryPath = WebConfig.getFilePath(courseCode,"courseDocument");
//        BaseCommonUtil.makeSureDictoryExists(dictoryPath);
//        String fileName = BaseCommonUtil.uuid() + "."+ext;
//        String filePath = dictoryPath+PATH_SPLIT + fileName;
//        try {
//            ImageUtil.GenerateImage(base64,filePath);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
////        CourseResource courseResource = storageApi.uploadCourseResource(courseCode, "courseDocument", multipartFile);
////        try {
////            CourseResource courseResource = storageApi.MyUpload(request, 4323,"courseDocument" );
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
//
//
//
//        return result;
//    }









    @Transactional
    @RequestMapping(value = "/uploadResource", method = RequestMethod.POST)
    public Result uploadResource(String courseCode,String showName,String myComment,HttpServletRequest request,@RequestParam("file")MultipartFile multipartFile)  {
        Result result = new Result<CourseResource>();
        if (courseCode==null||showName==null){
            result.setCode(101);
            result.setMsg("信息为空");
            return  result;

        }

        CourseResource courseResource = storageApi.uploadCourseResource(Integer.valueOf(courseCode), "courseDocument", multipartFile);
          if (courseResource!=null){
              courseResource.setResourceName(showName);
              if (!"".equals(myComment)){
                   courseResource.setResourceComment(myComment);
              }
              CourseResource hasSave = courseResourceService.saveResource(courseResource);
              if (hasSave!=null){
                  result.setMessage(hasSave);
                  result.setMsg("资源上传成功");
                  result.setCode(200);
                  return  result;
              }else{
                  result.setMsg("信息失败");
                  result.setCode(202);
              }

          }else{
             result.setMsg("文件上传失败");
             result.setCode(404);
          }


        return result;
    }




    @Transactional
    @RequestMapping(value = "/getCourseResource", method = RequestMethod.POST)
    public  Result getCourseResource(Integer courseCode){
        Result result = new Result<CourseResource>();
        List<CourseResource> courseResources = courseResourceService.findByCourseCode(courseCode);
        if (courseResources.size()>0){
            result.setMessage(courseResources);
            result.setMsg("找到课程资源了");
            result.setCode(200);
            return  result;
        }else{
            result.setMsg("没有找到课程资源");
            result.setCode(101);
            return  result;
        }

    }



    @Transactional
    @RequestMapping(value = "/deleteCourseResource", method = RequestMethod.POST)
    public  Result deleteCourseResource(Integer id){
        Result result = new Result<CourseResource>();
        CourseResource courseResource = courseResourceService.findResource(id);
        if (courseResource!=null){
            Boolean isdeleteFile = storageApi.deleteResource(courseResource);
            if (isdeleteFile){
                courseResourceService.deleteResource(id);
                result.setCode(200);
                result.setMsg("已从服务器删除资源");
                return  result;
            }else{
                result.setCode(199);
                result.setMsg("从服务器删除资源失败");
                return  result;
            }
        }else{
            result.setCode(404);
            result.setMsg("没有找到这个资源");
            return  result;
        }


    }



}
