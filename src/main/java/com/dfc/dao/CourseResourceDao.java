package com.dfc.dao;

import com.dfc.entity.CourseResource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CourseResourceDao extends JpaRepository<CourseResource,Integer>
        , PagingAndSortingRepository<CourseResource,Integer>
        , JpaSpecificationExecutor<CourseResource> {


    List<CourseResource> findByCourseCode(Integer courseCode);


}
