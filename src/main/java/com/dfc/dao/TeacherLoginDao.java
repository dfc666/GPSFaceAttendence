package com.dfc.dao;

import com.dfc.entity.TeacherLogin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TeacherLoginDao extends JpaRepository<TeacherLogin,Integer>
        , PagingAndSortingRepository<TeacherLogin,Integer>
        , JpaSpecificationExecutor<TeacherLogin> {

        TeacherLogin findByLoginCode(String loginCode);

}
